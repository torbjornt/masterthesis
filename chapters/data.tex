% !TeX root = ../main.tex
% !TeX spellcheck = en_GB


\chapter{Instruments and Data}
\label{chap:instruments}
\section{Instruments}
\label{sec:instruments}
A SeaGuard with a current meter was deployed to investigate the current inside the lagoon, and high frequency pressure sensors was deployed both inside and outside the lagoon to investigate both tidal variations of sea surface height and the wave conditions.

\subsection{SeaGuard}
\label{sec:seaguard}

SeaGuard is a platform from \gls{aadi} for mounting various instruments to a mooring.
For this work, only data from a \gls{rcm} is considered.
This \gls{rcm} has a \gls{dcs}, that uses the frequency shift of backscattered audio pulses to measure velocity.
The instrument transmits audio pulses at a given frequency.
When these pulses hit particles, \eg{} plankton, in the water, the signal is scattered, and a part of the scattered signal will go back to the instrument.
Due to the relative velocity of the particles to the instrument, this backscattered signal will have a different frequency than the original signal -- this is known as a Doppler shift.
Given the transmitted and backscattered frequency, and the speed of sound in water, the speed of the particle relative to the instrument can be calculated.


The mean accuracy of the \gls{dcs} is given as \SI{+-0.15}{\cm\per\s}, and it has a range of \SIrange{0}{300}{\cm\per\s}. \parencite{AanderaaDataInstruments2012}

\subsection{\rbr{} \textsc{tw}-2050}
\label{sec:rbr-tw2050}

The \textsc{tw}-2050 by \rbr{} is a pressure gauge with a temperature sensor, intended for mounting on a mooring at depths up to \SI{20}{\m}.
It measures pressure at preset intervals (the \enquote{tidal sampling period}), and if desirable it will compute an average over a shorter period (the \enquote{tidal averaging duration}), storing that mean value only.
For example if the tidal sampling period is set to \SI{10}{\minute}, and the tidal averaging duration to \SI{1}{\minute}, at the start of each \SI{10}{\minute} period the logger will record pressure for one minute, compute the average value, and store this.
For the next nine minutes, no readings are done \parencite{ruskinman}.

In addition, one can perform wave measurements at preset intervals, where this \enquote{wave sampling period} must be a multiple of the tidal sampling period.
These measurements are done at the specified rate, (\enquote{burst rate}, possible values are \SI{1}{\hertz}, \SI{2}{\hertz} and \SI{4}{\hertz}), and a specific number of samples (\enquote{burst length}, possible values are 512, 1024, 2048 or 4096) are stored.
For example, if the wave sampling period is set to \SI{30}{\minute}, the burst rate to \SI{4}{\hertz} and the burst length to 1024, every \SI{30}{\minute} the logger will make 1024 pressure readings at \SI{4}{\hertz}, and save all of them.
This means it will measure pressure for \( \SI[parse-numbers=false]{1024/4}{\second} = \SI{256}{\second} \), or just over four minutes \parencite{ruskinman}.


The logger has an accuracy of \SI{+-0.05}{\percent} of full scale for the pressure sensor, and \SI{+-0.002}{\degreeCelsius} for the temperature sensor, which has a range of \SIrange{-5}{35}{\degreeCelsius} \parencite{rbrdatasheet}.


% \subsubsection{Configuration}

% Prior to deployment, parameters for the instrument must be set.
% This is done by connecting it to a computer via an RS-232 interface % and using the supplied software, Ruskin.


% % \begin{table}
% %   \centering
% %   \caption{Parameters set in Ruskin for the TW-2050.}
% %   \label{tab:parameters}
% %   \begin{tabular}{lc}
% %     \toprule
% %     Tidal sampling period & \SI{3}{\minute} \\
% %     Tidal averaging duration & \SI{30}{\second} \\
% %     Wave sampling period & \SI{4}{\hour} \\
% %     Burst length & \num{2048} \\
% %     Burst rate & \SI{2}{\hertz} \\
% %     \bottomrule
% % \end{tabular}
% % \end{table}

% One can specify the time to start logging and stop logging, or set the % logger to start immediately.
% When a valid configuration is achieved, Ruskin displays a message % saying this, along with expected memory and battery usage.

% One must set the expected mean depth and the \enquote{Altitude}, the % height of the instrument above the sea floor, for the software to % calculate which periods and frequencies of waves one can expect to % resolve.
% Due to attenuation of the pressure signal, the deeper the instrument % is mounted, the  less of high frequency waves can be measured.
% Attenuation depends on the frequency of the signal as well as the % depth at which one measures, with higher frequencies having higher % attenuation, and attenuation increasing with greater depth % \parencite{gibbons}.


\subsection{GPS drifters}

The \gls{gps} units used for the drifters were a Garmin Astro system, designed to track dogs.
It consists of a Astro 320 handheld device and a \textsc{dc}40 \gls{gps} unit, originally attached to a dog's collar, mounted on the drifter.
The latter transmits its position to the handheld device via \textsc{vhf} every \SI{5}{\s}.

Small \gls{gps} units such as this typically have an accuracy of at best a few meters, depending on the satellite reception, atmospheric conditions and quality of the \gls{gps} device.



\section{Data}
\label{sec:data}

Two field campaigns have been conducted in Xai-Xai recent years. \parencite[Bilardo Nharreluga, personal communication]{nocathesis}
The first was in October 2011 near neap tide, the second in October 2012 near spring tide.


\subsection{2011 campaign}

Waves were measured using three different instruments, at different locations.
At the beach, an ultrasonic altimeter mounted above the surface looking down.
Further out in the lagoon, and outside the reef, an accelerometer mounted on a buoy, and an \rbr{} \textsc{tw}-2050 were used.

Surface current speeds were measured using \gls{gps}-drifters, see \cref{tab:drifters2011} for an overview.
A total of eight drifter deployments were done, three on October 18, two on October 19, and three on October 20. 
No current meters were deployed during this period.

\begin{table}
  \centering
  \caption{Overview of drifter deployments in 2011.}
  \label{tab:drifters2011}
  \input{tables/driftertable2011}
\end{table}

In addition, wave setup on the beach were measured using pressure tubes, and a small weather station measured atmospheric conditions.

For this thesis, only data from the drifters will be considered.

\subsection{2012 campaign}

Similarly to 2011, waves were measured with an acoustic transponder by the beach, an accelerometer on a buoy, and with pressure sensors from \rbr.
This time, two similar pressure sensors were deployed, to obtain concurrent measurements inside and outside the reef.
Averaged quantities was measured every \SI{5}{\minute} (the \enquote{tidal sampling period}, cf. \cref{sec:rbr-tw2050}), wave measurements were done every \SI{20}{\minute} (the \enquote{wave sampling period}).

In addition, a SeaGuard was deployed to measure the current inside the lagoon.
This was mounted close to the surface, on the same mooring as the pressure sensor.
The position of the mooring is indicated on \cref{fig:instrument-pos}.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{figures/maps/GEarth-XX-instruments}
  \caption[Aerial photo of the lagoon at Xai-Xai.]{Aerial photo of the lagoon
   at Xai-Xai \parencite{GEarth-XX-instruments}, with the position of the instruments indicated. The SeaGuard was mounted on the same mooring as the \rbr{} \textsc{tw}-2050, inside the lagoon.
  }
  \label{fig:instrument-pos}
\end{figure}
% 
Three \gls{gps}-drifters were also deployed on the morning of October 31, providing Lagrangian measurements of surface current.
\cref{tab:drifters2012} has a summary of these deployments.

\begin{table}
  \centering
  \caption{Overview of drifter deployments in 2012.}
  \label{tab:drifters2012}
    \input{tables/driftertable2012}
\end{table}

Atmospheric conditions were again measured with a small weather station mounted on the beach.

For security purposes, the instruments were usually redeployed each day, but one full day and night with  concurrent measurements of waves and current velocity was obtained, on October 30 to October 31.


\section{ERA-Interim reanalysis}
\label{sec:era-inter-rean}

\gls{erai} is a global reanalysis from the \gls{ecmwf}, providing data from 1979 until today, being continuously updated.
It provides gridded values of a wide range of parameters describing the atmosphere as well as the land and ocean surface.
One of the components is a wave model with a \SI{110}{\kilo\m} horizontal resolution, providing a large range of parameters describing the surface wave field.

Time series of some of these parameters, listed in \cref{tab:eraiparams}, was made available, from two grid points close to Xai-Xai, indicated in \cref{fig:era-gridpoints}.
One of these was close to the coast, about \SI{50}{\km} east of Xai-Xai, while the second was roughly \SI{200}{\km} further away from the coast, south-south-east of the first.
These time series has a \SI{6}{\hour} sampling interval.
The data used here are from January 1, 2011 to December 31, 2012.


\begin{figure}
  \centering
  \includegraphics[width=9cm]{./figures/maps/ERA-gridp-map}
  \caption{Position of the two grid points P1 and P2 from which ERA-I data was obtained.}
  \label{fig:era-gridpoints}
\end{figure}

\begin{table}
\centering
\caption{Parameters from \gls{erai}.}
\label{tab:eraiparams}
\begin{tabular}{ll}
\toprule
SWH & Significant wave height [m] \\
MWD & Mean wave direction [degrees] \\
PP1D & Peak period of 1D spectra [s] \\
MWP & Mean wave period [s] \\
CDWW & Coefficient of drag with waves \\
SHWW & Significant height of wind waves [m] \\
MDWW & Mean direction of wind waves [degrees] \\
MPWW & Mean period of wind waves [s] \\
SHPS & Significant height of primary swell [m] \\
MDPS & Mean direction of primary swell [degrees] \\
MPPS & Mean period of primary swell [s] \\
\( u \) & East-west wind component \\
\( v \) & North-south wind component \\
\bottomrule
\end{tabular}
\end{table}

\section{\tpx{} tidal model}
\label{sec:tpxo-tidal-model}

As a substitute for measurements of water level, the \tpx{} tidal model  was used to provide predictions of tidal amplitudes near Xai-Xai for all of 2011 and 2012, concurrent with the data from \gls{erai}.
\tpx{} is a global inverse tidal model with a resolution of \( 1/4^\circ \times 1/4^\circ \). \parencite{egbert2002efficient}
Using the \ML{} interface called \textsc{tmd}, it was run for the coordinates of the \gls{erai} grid point closest to the coast, \ie{} P1 (see \cref{fig:era-gridpoints}), providing a time series with hourly values of sea surface height.







% %%% Local Variables: 
% %%% mode: latex
% %%% TeX-master: "../main"
% %%% ispell-local-dictionary: "british"
% %%% End: