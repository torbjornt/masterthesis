% !TeX program = pdflatex
% !TeX root = ../main.tex
% !TeX spellcheck = en_GB


\chapter{Results}
\label{chap:results}

\section{Measurements}

From the campaign in 2011, only data from \gls{gps} drifters are shown here, in \cref{fig:2011drifters}.
Generally, the drifters moved along the beach, roughly east-north-east.
This drift is, however, not entirely continuous in that most drifters occasionally turn around for short periods, track 4 displays this most clearly.
In some cases, track 1 in particular, the path of the drifter was relatively short, and more northbound, these probably drifted ashore.
Indicated in the figure is also the position of the SeaGuard in 2012, for reference.

\begin{figure}
  \centering
  \includestandalone{./figures/tracks/2011tracks}
  \caption[Plot of \textsc{gps} tracks from 2011]{Plot of the  \gls{gps} tracks from the field campaign in 2011.
  All drifters moved either east-north-east or north.
  While there was no SeaGuard deployed in 2011, the position of the   SeaGuard in 2012 is indicated by the red dot, for reference.
The dashed grey line indicate the approximate position and orientation of the reef.
Track numbers correspond to those in \cref{tab:drifters2011}.}
  \label{fig:2011drifters}
\end{figure}

\Cref{fig:2012drifter_SG} show the tracks and speed of \gls{gps} drifters in 2012, both compared to the SeaGuard.
All the drifters move east-north-east, more or less parallel to the beach.
Their speed fluctuates greatly, but the mean value, indicated by red dots in \cref{fig:rbrvsdrifters12}, is generally higher than the current speed measured by the SeaGuard.

\begin{figure}
  \centering%
  \subbottom{\includestandalone{./figures/plots/gps-vs-seaguard-pgfpl}\label{fig:rbrvsdrifters12}}
  \subbottom{\includestandalone{./figures/tracks/2012tracks}\label{fig:driftertracks12}}%
  \caption{Comparisons of the SeaGuard and \gls{gps} drifters. \subcaptionref{fig:rbrvsdrifters12} shows the speed measured by the SeaGuard and the drifters in October 2012; red dots indicate the mean speed of the drifters.
  \subcaptionref{fig:driftertracks12} shows the \gls{gps} tracks and position of SeaGuard.
The dashed grey line indicate the approximate position and orientation of the reef.
Track numbers correspond to those in \cref{tab:drifters2012}.
  }
  \label{fig:2012drifter_SG}
\end{figure}

As mentioned above, not only the magnitude, but also the direction of the drifters can change.
While the general direction of movement is east-north-east, there are short periods where the drifters move the other way, as seen in \cref{fig:drifterspeed}, which shows the alongshore speed  of some of the drifters.
Mostly the speed is positive, \ie{} east-north-east, but there are exceptions, for example in \cref{fig:ts3}.
The tracks showed in \cref{fig:drifterspeed} are of those with the clearest west-east propagation.
Hence, track 1 from 2011 (cf. \cref{fig:2011drifters}) for example is \emph{not} included.

\begin{figure}
  \centering%
  \subbottom{\label{fig:ts1}}%
  \subbottom{\label{fig:ts2}}%
  \subbottom{\label{fig:ts3}}%
  \subbottom{\label{fig:ts4}}%
  \subbottom{\label{fig:ts5}}%
  \subbottom{\label{fig:ts6}}%
  \subbottom{\label{fig:ts7}}%
  \subbottom{\label{fig:ts8}}%
  \includestandalone{figures/plots/drifterspeed}
  \caption{Alongshore speed of  selected drifters.
  The \( x \)-axis shows alongshore distance travelled by the drifters.}
  \label{fig:drifterspeed}
\end{figure}

\Cref{fig:2012-data} shows time series of alongshore velocity measured by the SeaGuard, and the sea level (deviation from mean) and significant wave height, measured by an \rbr{} \textsc{tw}-2050.
They are about 26 hours long, starting shortly after a low tide, covering just over two complete tidal cycles.
The current speed varies along with the sea level, being low at low tide, and higher during high tide.
The significant wave height is almost constant at a little more than \SI{1}{\m} during most of the time series, but increases to almost \SI{3}{\m} during the last 8--9 hours. 

\begin{figure}
  \centering
  \includestandalone{figures/plots/2012-hs_u_eta}
  \caption{Time series of the along shore velocity \( u \), sea surface anomaly outside the lagoon \( \eta \) and significant wave height \( H_s \) outside the lagoon.
From October 30--31, 2012.}
  \label{fig:2012-data}
\end{figure}


\subsection[Comparison of \textsc{era-i} and \rbr]{Comparison of \gls{erai} and \rbr{}}
\label{sec:comparison-era-rbr}

\Cref{fig:eraVsrbr} shows the data from the \rbr, measured in October and November 2012, and the data from the \gls{erai} reanalysis.
The grey shading indicates the time when the SeaGuard was deployed inside the lagoon.
For the significant wave height, the \gls{erai} data fit quite well with the \rbr{} measurements, with the exception of the first half day of measurements, where the \gls{erai} data are about \SI{0.5}{\m} higher than the \rbr.
Both time series vary very little during this period, but \gls{erai} is about \SI{0.5}{\m} higher than the \rbr.

For these days, both the mean wave period and mean period for primary swell fit quite well with the measurements, the largest differences are for the latter, in the middle of the time series.
This good fit indicates that swell dominates the wave field.
Comparisons of other parameters from \gls{erai} (\cref{tab:eraiparams}), \eg{} significant height and mean period for wind waves vs. primary swell (not shown), support this.

\begin{figure}
  \centering
  \subbottom{\label{fig:rbrvsera-swh}}%
  \subbottom{\label{fig:rbrvsera-wp}}%
  \includestandalone{figures/plots/rbr_Vs_ERA}
  \caption{A comparison of measurements done with an \rbr{} pressure gauge in 2012, and data from the \gls{erai} reanalysis.
  \subcaptionref{fig:rbrvsera-swh} shows significant wave height, \subcaptionref{fig:rbrvsera-wp}  shows wave period.
  The grey shading indicate the period where concurrent measurements of current velocity and wave height were done (see \cref{fig:2012-data}).}
  \label{fig:eraVsrbr}
\end{figure}

\subsection{Comparison of \tpx{} and \rbr}
\label{sec:comparison-tpx-rbr}

\Cref{fig:rbr_vs_tmd} shows a comparison of the \tpx{} tidal model and measurements made by the \rbr{} pressure gauge.
While they do not match exactly, the phase and amplitude fit quite well.
There is a slight increasing trend in mean sea level in the measurements not seen in the model, which can be caused by effects other than tides.
Using \tpx{} as a substitute for sea surface height seems to be a decent approximation.


\begin{figure}
  \centering
  \includestandalone{figures/plots/rbr_vs_TMD}
  \caption{Comparison of measurements done with an \rbr{} pressure gauge in 2012, and sea surface height from the \tpx{} tidal model, for sea surface height. }
  \label{fig:rbr_vs_tmd}
\end{figure}

\section{Zero dimensional model}
\label{sec:zero-dimens-model}

To force the model, a simple sine curve was used for the exterior sea level, \( \eta \), and the mass flux due to waves was set to a constant.
The parameters used are given in \cref{tab:0dparams}.


\Cref{fig:zerod-nodata} shows modelled time series of current speed and sea surface height.
There are some wiggles at the beginning, which is an adjustment from the initial values given.

The interior sea level, \( \zeta \), follows the exterior sea level closely.
When \( \eta>\hr \) the current follows a smooth, sine-like curve.
During the ebb of the tide, the current show a short increase just after the level of \( \eta \) becomes \( \hr \), before it continues decreasing, though now in a more linear fashion.
There are also some small oscillations visible in the current speed immediately after \( \eta \) reaches \( \hr \).
The decrease continues until \( \eta \) again reaches  \( \hr \), during the flood of the tide, at this point there is a sharp increase.

For the time when \( \eta < \hr \), only \(\eta\) influence the interior sea level.
Until \(\eta\) reaches low tide, the current is positive, so the lagoon is drained of water.
After low tide, as the sea level starts rising, the current becomes negative, meaning that water is running back into the lagoon.
\begin{table}
\begin{minipage}{.45\linewidth}
  \centering
  \caption{Model parameters for Case 1 of \mD{0}-model (\cref{fig:zerod-nodata}).}
  \label{tab:0dparams}
  \begin{tabular}{*{5}{>{\(} c <{\)} }}
    \toprule
    \alpha & \gamma & \epsilon & b & \hr \\
    10 & 10 & 0.25 & \SI{20}{\m}  & \SI{-0.5}{\m}\\
    \bottomrule
  \end{tabular}
\end{minipage}\hfill
\begin{minipage}{.45\linewidth}
  \centering
  \caption{Model parameters for Case 2 of the \mD{0}-model (\cref{fig:0dwideopening}).}
\label{tab:0dwideopeningparams}
  \begin{tabular}{*{5}{>{\(} c <{\)}}}
    \toprule
    \alpha & \gamma & \epsilon & b & \hr \\
    1 & 5 & 0.6 & \SI{100}{\m} & \SI{-0.5}{\m} \\
    \bottomrule
  \end{tabular}
\end{minipage}
\end{table}

\begin{figure}
  \centering
  \includestandalone{./figures/plots/zerod_u_zeta}
  \caption{Current velocity and sea surface height in the lagoon from Case 1 of the \mD{0}-model (parameters as defined in \cref{tab:0dparams}).}
  \label{fig:zerod-nodata}
\end{figure}


Changing the setup of the model can give quite different results, cf. \cref{fig:0dwideopening,tab:0dwideopeningparams}.
Notably, increasing the width of the opening or reducing \( \hr \) will reduce the magnitude of the negative current seen just after low tide.
The small \enquote{wiggles} that are seen in the previous case, \cref{fig:zerod-nodata} are more pronounced here.

Also evident from both these cases is that the maximum value of the current speed occurs after the maximum value of the sea surface height.
This \enquote{lag} is larger for the first case than the last case, at about \SI{110}{\minute} and \SI{45}{\minute} respectively.

\begin{figure}
  \centering
  \includestandalone{./figures/plots/zerod-idealcase}
  \caption{Current velocity and sea surface height in the lagoon from the Case 2 of \mD{0}-model (parameters as defined in \cref{tab:0dwideopeningparams}).}
  \label{fig:0dwideopening}
\end{figure}

\section{One dimensional model}
\label{sec:one-dimens-model}

For this chapter, only ideal cases are considered.
That is, the driving forces of the system is a constant significant wave height, and a sinusoidal exterior sea surface height.
The latter is the sum of two different sine functions, with periods equivalent to the \( M_2 \) and \( S_2 \) tides, and a combined amplitude of \SI{1}{\m}, at most.

All time series are from a grid point close to the centre of the lagoon, unless otherwise stated.
This grid point, was chosen based on the  location of the SeaGuard in the campaign of 2012, cf. \cref{fig:instrument-pos}.
The centre of the grid cell, which is where \( \zeta \) is given, is at \( \SI{875}{\m} \). 
Due to the staggering and grid length of \SI{50}{\m}, \( u \) is given at \( x = \SI{900}{\m} \), which is the right edge of the cell (\cref{fig:1D-grid}).

Originally, the \mD{1} model had a wave forcing similar to the \mD{0} model, \ie{} proportional to \( \max(\eta - \hr, 0) \).
With this \enquote{old} version of the wave forcing, the current time series has a bell-like shape for water levels above \( \hr \), and nearly constant, close to zero, for water levels below \( \hr \).
\Cref{fig:1Dideal-oldflux} shows the results with \( \hr = \SI{-0.5}{\m} \).
This demonstrates that the current speed increases as long as the tide is flooding and \( \eta > \hr \), starting a decrease only as the ebbing starts.


\begin{figure}
  \centering
  \includestandalone{./figures/plots/1D-ideal-oldflux}
  \caption{Current speed and sea surface height from the \mD{1} model, with the \enquote{old} version of mass flux, as in \cref{eq:0D-zeta-final}.}
  \label{fig:1Dideal-oldflux}
\end{figure}

With the \enquote{new} version from \cref{eq:1D-waveflux}, the current behaves differently, having a local minimum at high tide, see \cref{fig:1Dideal-newflux}
In addition, the oscillations in current speed at low tide, while present, have a much smaller amplitude.


\begin{figure}
  \centering
  \includestandalone{./figures/plots/1D-ideal}
  \caption{Current speed \( u \) and sea surface height \( \zeta \) from the \mD{1} model, with the \enquote{new} version of mass flux, \cref{eq:1D-waveflux}.}
  \label{fig:1Dideal-newflux}
\end{figure}


In both cases, there are some irregularities in the beginning of the time series, similar to the \mD{0} case, but less pronounced.
Again, these are a result of the model adjusting from the initial values, and they are seen clearly only during this adjustment phase.


\subsection{Varying the parameters -- time series}
\label{sec:paramvar}

This section will show how the \mD{1} model responds to variations in the forcing, as well as other parameters.
The default set of parameters is given in \cref{tab:vardefaultparams}.
Time step and grid length was set to \SI{1}{\s} and \SI{50}{\m}, respectively.
Note that, as mentioned in the previous section, the exterior sea level \( \eta \), which is the same as that in \cref{fig:1Dideal-newflux}, has an amplitude of at most \SI{1}{\m}.

\begin{table}
  \centering\small
  \caption{Default set of parameters for model results in \cref{sec:paramvar}.}
  \label{tab:vardefaultparams}
  \begin{tabular}{*{9}{>{\(} c <{\)}}}
    \toprule
    H_s &\epsilon  & T & \lambda & r & h_b & h_t & h_b' & h_t' \\
    \SI{2}{\m} & 1 & \SI{10}{\s} & \SI{0.1}{\m\per\s} & \SI{0.001}{\per\s} & \SI{-0.7}{\m} & \SI{0.4}{\m} & \SI{-0.4}{\m} & \SI{0.4}{\m} \\
    \bottomrule
  \end{tabular}
\end{table}

\Cref{fig:hsvar} shows how the model responds to forcing by wave fields with different significant wave heights.
For wave heights larger than between \SI{2}{\m} and \SI{3}{\m} the sea surface height becomes rather large, as much as \SI{0.7}{\m} above the exterior sea level for the case with waveheights of \SI{3.3}{\m}.
The maximum current velocity around high tide increases with increasing wave height, while at low tide there is no difference.

\begin{figure}
  \centering
  \subbottom{\label{fig:hsvar-u}}%
  \subbottom{\label{fig:hsvar-zeta}}%
  \includestandalone{figures/plots/varplot/Case1hsvar}
  \caption{Idealised case, with different wave heights.
  \subcaptionref{fig:hsvar-u} shows time series for current speed \( u \), \subcaptionref{fig:hsvar-zeta} shows sea surface height \( \zeta \).}
  \label{fig:hsvar}
\end{figure}


\Cref{fig:epsvar} has a similar plot, but with different values for \( \epsilon \), the coefficient for the wave generated cross reef flow (\cref{eq:1D-waveflux}).
As expected, higher values of \( \epsilon \) lead to higher current speeds, and higher maxima for \( \zeta \). 

\begin{figure}
  \centering
  \subbottom{\label{fig:epsvar-u}}%
  \subbottom{\label{fig:epsvar-zeta}}%
  \includestandalone{figures/plots/varplot/Case1epsvar}
  \caption{Idealised case, with different values for \( \epsilon \).
  \subcaptionref{fig:epsvar-u} shows time series for current speed \( u \), \subcaptionref{fig:epsvar-zeta} shows sea surface height \( \zeta \).}
  \label{fig:epsvar}
\end{figure}



In \cref{fig:Tvar} it is the wave period that is changed.
The wave period is used to calculate the frequency and wave number, both of which are used in \cref{eq:bg-stokesvelocity} to find a mass transport velocity.
Of the four cases shown, the lowest current speed and sea surface height is for the second shortest period, \SI{8}{\s}.
Both the shorter period, \SI{5}{\s} and the two longer periods, \SI{11}{\s} and \SI{14}{\s}, period show higher current speeds and sea surface height at high tide.
% In some cases, such as \cref{fig:Tvar-u} at  \( t \approx % \SI{12}{\hour} \), there are some small oscillations in the current % speed just as \(\eta\) reaches \( h_t \).
% This is a similar feature to that seen near \( \hr \) for the \mD{0} % model (cf. \cref{sec:zero-dimens-model}).




\begin{figure}
  \centering
  \subbottom{\label{fig:Tvar-u}}%
  \subbottom{\label{fig:Tvar-zeta}}%
  \includestandalone{figures/plots/varplot/Case1Tvar}
  \caption{Idealised case, with different values for wave period.
  \subcaptionref{fig:Tvar-u} shows time series for current speed \( u \), \subcaptionref{fig:Tvar-zeta} shows sea surface height \( \zeta \).}
  \label{fig:Tvar}
\end{figure}

In \cref{fig:lambdavar} the varied parameter is \(\lambda\), the coefficient for the cross-reef mass flux driven by sea level difference.
There is little change in the sea level at high tide when changing \(\lambda\), but higher \(\lambda\) leads to lower current speeds.
Note that this term may have more significant effects for other conditions, as it depends on the difference \( \eta - \zeta \), cf.~\cref{eq:crossreef_pressureflux}.
Hence, in conditions with large \( \zeta \) this term will be larger as well.

\begin{figure}
  \centering
  \subbottom{\label{fig:lambdavar-u}}%
  \subbottom{\label{fig:lambdavar-zeta}}%
  \includestandalone{figures/plots/varplot/Case1lambdavar}
  \caption{Idealised case, with different values for \(\lambda\).
  \subcaptionref{fig:lambdavar-u} shows time series for current speed \( u \), \subcaptionref{fig:lambdavar-zeta} shows sea surface height \( \zeta \).}
  \label{fig:lambdavar}
\end{figure}

When changing the friction coefficient \( r \), shown in \cref{fig:rvar}, a lower value led to higher current speeds, and a lower maximum for sea surface height.
For all cases, the sea surface height is somewhat higher than the \SI{1}{\m} maximum of the exterior sea surface height.
The largest difference for the largest friction coefficient, when the sea surface height at high tide is about \SI{30}{\centi\m} higher than \(\eta\).


\begin{figure}
  \centering
  \subbottom{\label{fig:rvar-u}}%
  \subbottom{\label{fig:rvar-zeta}}%
  \includestandalone{figures/plots/varplot/Case1rvar}
  \caption{Idealised case, with different values for \( r \).
  \subcaptionref{fig:lambdavar-u} shows time series for current speed \( u \), \subcaptionref{fig:lambdavar-zeta} shows sea surface height \( \zeta \).}
  \label{fig:rvar}
\end{figure}


\Cref{eq:F(eta)} defines two limiting levels for the wave mass flux, \( h_b \) and \( h_t \), plots for different values of these are in \cref{fig:hbhtvar}.
These are defined relative to the mean sea level.
The former defines the lowest exterior sea level when inflow will occur, while the latter defines the lowest level where maximum inflow occurs.
Neither of them influences the maximum sea level at high tide.
They do however cause some small differences in the sea level between high and low tide.
Higher values of \( h_b \) gives a slightly lower sea level near the zero level.
For \( h_t \) the differences are more apparent, but not very large.
With \( h_t \) at the lowest value, \SI{0}{\m}, \( \zeta \) is relatively high near \( \eta=\SI{0}{\m} \).
At both high and low tide there is very little or no difference.

For the current speed, \( h_b \) decides when it will start increasing, during the flood of the tide.
The lower this level is placed, the closer to low tide will the current start increasing.
The other level, \( h_t \), decides when the maximum inflow occurs.
If this level is the same as the exterior sea level at high tide, the current speed has a sine-like curve with a maximum at high tide.
If it is below the exterior sea level at high tide however, the current speed has a local minimum at high tide, with maxima before and after.
Defining \( h_t \) to a lower value leads to shifting these maxima away from the time of high tide. 

\begin{figure}
  \centering
  \subbottom{\label{fig:hbvar-u}}%
  \subbottom{\label{fig:hbvar-zeta}}%
  \subbottom{\label{fig:htvar-u}}%
  \subbottom{\label{fig:htvar-zeta}}%
  \includegraphics{figures/plots/varplot/Case1hbhtvar}
  \caption{Idealised case, with different values for \(h_b\) [\subcaptionref{fig:hbvar-u} and \subcaptionref{fig:hbvar-zeta}] and \( h_t \) [\subcaptionref{fig:htvar-u} and \subcaptionref{fig:htvar-zeta}].
  Upper panels show time series for current speed \( u \), lower panels show sea surface height \( \zeta \).}
  \label{fig:hbhtvar}
\end{figure}

Finally, the two limits for the pressure driven cross reef flow, defined in \cref{eq:fout(eta)}, \cref{fig:gbgtvar} shows plots for different values of these.
Note that for this case, the value of \( \lambda \) was set to \SI{1}{\m\per\s}, to make the differences more pronounced.
Changing either makes very little difference for \( \zeta \) with the given conditions.
For the current speed, the largest effects are seen for the highest values of both \( h'_b  \) and \( h'_t \).
With \( h'_b = \SI{0}{\m} \) the current speed increases faster during flooding, until a point where this term becomes higher than zero, and the acceleration is reduced.
Generally, a lower value of \( h'_b \) seems to give a more constant acceleration during both ebb and flood.

For the upper limit \( h'_t \), a lower value gives a lower current speed midway between high and low tide.
In \cref{fig:gtvar-u} this is seen where the blue curve is a little lower than the red and green curves.
With the highest value for \( h'_t \), \SI{1}{\m}, the local maxima before and after high tide are also higher.
This is because in the other two cases, \( h'_t \) was below, or close to, \( h_t \), so that pressure driven cross reef flow has already reached its highest value, or very nearly, so that the conditions are similar for \( \eta > h'_t \).
With \( h'_t = \SI{1}{\m} \) however, the contribution from this term is significantly low enough also for higher \( \eta \), to give a notably higher current speed.

\begin{figure}
  \centering
  \subbottom{\label{fig:gbvar-u}}%
  \subbottom{\label{fig:gbvar-zeta}}%
  \subbottom{\label{fig:gtvar-u}}%
  \subbottom{\label{fig:gtvar-zeta}}%
  \includegraphics{figures/plots/varplot/Case1gbgtvar}
  \caption{Idealised case, with different values for \(h_b\) [\subcaptionref{fig:gbvar-u} and \subcaptionref{fig:gbvar-zeta}] and \( h_t \) [\subcaptionref{fig:gtvar-u} and \subcaptionref{fig:gtvar-zeta}].
  Upper panels show time series for current speed \( u \), lower   panels show sea surface height \( \zeta \).
For this case, \( \lambda \) was set to \SI{1}{\m\per\s}, to make the effects more visible.}
  \label{fig:gbgtvar}
\end{figure}


\subsection{Spatial variability}

The current speed generally increases towards the opening, see \cref{fig:u(x)}.
This also applies for the short period around low tide when the current is negative, \ie{} water is flowing into the lagoon -- the strongest current is closest to the opening.
Time series taken from different grid points, shown in \cref{fig:u(tx)}, also demonstrate these features, with considerably higher current speeds near high tide for the grid point closer to the opening, as well as stronger current into the lagoon near low tide.
All three time series demonstrate the same behaviour, the difference lies in the amplitude of the signal.

For the sea surface height, \( \zeta \), the largest values at high tide are furthest into the lagoon, see \cref{fig:u(x)}.
A similar plot for low tide is not shown, but it would show the opposite structure -- lower values further into the lagoon and higher closer to the opening -- with a much smaller magnitude.
However, the difference is much smaller than for the case at high tide.
\Cref{fig:u(x)} shows that the spatial gradients of current speed and sea surface height have opposite signs.


\begin{figure}
\centering
\includestandalone{figures/plots/u(x)}
\caption{Spatial variability of sea surface height \( \zeta \) at high tide (upper panel), and modelled current speed \( u \)  at high tide and low tide (middle panel), for an ideal case.
The lower panel shows the width of the model lagoon.
The dotted blue line is at \( x = \SI{875}{\m} \), indicating the location of the grid cell used for the time series above.}
\label{fig:u(x)}
\end{figure}

\begin{figure}
\centering
\includestandalone{figures/plots/u(x)timeseries}
\caption{Modelled current speed \( u \) for an ideal case, at three different grid points, number 2, 18 and 32. The total number of grid points is 34.}
\label{fig:u(tx)}
\end{figure}
\begin{figure}
\centering
\includestandalone{figures/plots/zeta(x)timeseries}
\caption{Modelled current speed \( u \) for an ideal case, at three different grid points, number 2, 18 and 32. The total number of grid points is 34.
Also shown is the exterior sea level, \( \eta \).}
\label{fig:zeta(tx)}
\end{figure}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
