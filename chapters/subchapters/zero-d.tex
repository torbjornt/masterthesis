% !TeX root = ../../main.tex
% !TeX spellcheck = en_GB


\section{Zero dimensional description}

This description mainly follows \textcite{kai-model}, and has only a time dependency, no variation in space.
\Cref{fig:0D-mathmod} shows a schematic of the system.
It is a simplified system, where water enters the lagoon by spilling over the reef, and exits the lagoon through a single opening.
The width \( B \) and depth is  assumed constant.
The alongshore flow \( u \) is assumed to be independent of depth, and horizontally uniform.


\begin{figure}
  \centering
  \includestandalone{figures/mathmod/0D-schematic}
  \caption{Schematic of the lagoon, as in \textcite{kai-model}.}
  \label{fig:0D-mathmod}
\end{figure}

There are two unknowns in this system, the current velocity \( u \), and the sea level \( \zeta \), both on the inside of the lagoon.
Forcing the system is incoming waves and the sea level outside the lagoon.
Both \( \zeta \) and \( \eta \)  are given as the deviation from the mean sea level
Hence, the total water depth inside the reef is given by \( h_\mathrm{total, in} = H_\mathrm{in} + \zeta\), where \( H_\mathrm{in} \) is the mean water depth in the lagoon. 
Similarly for outside the lagoon, we get \( h_\mathrm{total, out} = H_\mathrm{out} + \eta\), see \cref{fig:crossection}.

The two equations describing the system are for conservation of volume and momentum.
A summary of the parameters defining the system is found in \cref{tab:lagoonparameters}.

\begin{figure}
  \centering
  \includestandalone{figures/mathmod/cross-section}
  \caption{Schematic cross section of reef, indicating different depths for a situation near low tide.}
  \label{fig:crossection}
\end{figure}

\begin{table}
  \centering
  \caption{Parameters describing the \mD{0}-system.}
  \label{tab:lagoonparameters}
  \begin{tabular}{>{\(} l <{\)} l}
  \toprule
  \multicolumn{1}{l}{Parameter} & Description \\ \midrule
  V     & Cross-reef volume transport per unit length \\
  L     & Length of lagoon \\
  B     & Width of lagoon \\
  b     & Width of opening in reef \\
  \zeta & Sea-level inside the lagoon, deviation from mean \\
  \eta  & Sea-level outside the lagoon, deviation from mean \\
  u     & Current velocity inside the lagoon \\
  \hr & Lower level for wave flux \\
  \bottomrule
\end{tabular}
\end{table}

\subsection{Conservation of volume}

The change of volume in the lagoon, \( \Delta \mathcal{V} \), during a time \( \Delta t \), is the difference between the inward flux of water over the reef, and the outward flux through the opening, \ie
%
\begin{equation}\label{eq:0D-dV1}
  \Delta \mathcal{V} = VL\Delta t - (\zeta + H_{\mathrm{in}}) bu\Delta t,
\end{equation}
%
where the terms on the right hand side is as described in \cref{tab:lagoonparameters}.
Also, given constant geometry of the lagoon, the change in volume will be given by the change in sea level inside the lagoon, \( \Delta\zeta \), and the area.
Hence,
%
\begin{equation}\label{eq:0D-dV2}
  \Delta \mathcal{V} = LB\Delta \zeta.
\end{equation}
%
By equating the right hand sides of \cref{eq:0D-dV1,eq:0D-dV2}, and dividing by \( LB\Delta t \), we get a prognostic equation for \( \zeta \).
%
\begin{equation}
  \frac{\diff \zeta}{\diff t} = \frac{V}{B} - (\zeta + H_{\mathrm{in}}) \frac{b}{BL}u.
\end{equation}
%
For small \( \Delta t \), \( \Delta\zeta/\Delta t \to \diff\zeta/\diff t\).

The wave-induced cross-reef volume transport, \( V\! \), is given as a product of a surface mass transport velocity \( U_0 \)  and a function of \( \eta \).
\( U_0 \) is given by \cref{eq:bg-stokesvelocity}, evaluated for \( z=0 \).
The dependency on \(\eta\) is given as
%
\begin{equation}
  f(\eta) = \max(\eta-h_\mathrm{ref},0).
\end{equation}
%
Hence, when the sea level is below a given height \( h_\mathrm{ref} \), there is no inflow.
With the addition of a free parameter \(\epsilon\), \( V \) is given as
%
\begin{equation}\label{eq:zeroD-V}
  V = \epsilon U_0\cdot\max(\eta-h_\mathrm{ref},0).
\end{equation}
%
The free parameter \( \epsilon \) is used as an adjustment for the inflow, should it appear to be too low or too high.



\subsection{Momentum}
As the model is for a single point, there are no advective terms, and the total derivative of the velocity can be replaced by \( \diff u/\diff t \).
Rotational effects are ignored, as the size of the system is small.
The pressure gradient force is assumed to be proportional to the difference in sea level between the inside and the outside, \ie
%
\begin{equation}
  -\frac{1}{\rho}\nabla p \approx \beta (\zeta - \eta),
\end{equation}
%
where \( \beta \) is a free parameter.
Constant density is assumed.

A frictional force per unit mass is assumed to be proportional to the current velocity,
%
\begin{equation}\label{eq:0d-friction}
  \frac{1}{\rho}\mathsf{F} \approx -ru,
\end{equation}
%
where \( r \) is a friction coefficient.
This gives a form of the momentum equation:
%
\begin{equation}\label{eq:0D-momentum}
  \frac{\diff u}{\diff t} = \beta(\zeta - \eta) - ru.
\end{equation}


\subsection{Scaling}

In \cref{eq:0D-momentum} there are two unknown coefficients, \( \beta \) and \( r \).

Assuming hydrostatic pressure, the pressure gradient force \( F_\text{p} \) scales like
%
\begin{equation*}
  F_\text{p} \sim g\frac{\text{difference in sea level}}{\text{typical length scale}}.
\end{equation*}
%
Hence, \( \beta \) will scale like
%
\begin{equation}
  \beta \sim \frac{g}{L}.
\end{equation}

The friction coefficient \( r \) must have the unit \si{\per\second}.
By introducing \( T \) as half the tidal period, using this as a time scale, and assuming that the frictional force is weak, \( r \) will scale as
%
\begin{equation}
  r \sim \frac{1}{T}.
\end{equation}

We can further introduce dimensionless parameters \( \alpha = \beta L/g \) and \( \gamma = rT \), both of which should be \( \mathcal{O}(1) \), to get the final forms of the governing equations, 
%
\begin{subequations}\label{eq:0D-finaleq}
\begin{align}
  \frac{\diff\zeta}{\diff t} &= \frac{1}{B}
    \left[
      \epsilon \cdot \max(\eta - \hr,0)\cdot U_0 -
      \frac{b}{L}(\zeta + H_{\mathrm{in}}) u
    \right]  \label{eq:0D-zeta-final}  \\
  \frac{\diff u}{\diff t} &= \alpha \frac{g}{L}(\zeta - \eta) - \frac{\gamma}{T} u.
  \label{eq:0D-ufinal}
\end{align}
\end{subequations}



\subsection{Discretisation}

The equations \eqref{eq:0D-finaleq} are discretised with a forward-backward scheme, staggered in time, giving the \glspl{fde}
%
\begin{subequations}
 \begin{align}
    \frac{\zeta^{n+1} - \zeta^{n}}{\Delta t} &= \frac{1}{B}\left[
      \epsilon \cdot \max(\eta^n-\hr,0) \cdot U_0^n
      - \frac{b}{L}(\zeta^n + H_{\mathrm{in}}) u^n
      \right] \\
    \frac{u^{n+1} - u^n}{\Delta t} &= \alpha\frac{g}{L}(\zeta^{n+1} - 
      \eta^{n+1}) - \frac{\gamma}{T}u^n.
 \end{align}
\end{subequations}
%
For the case of a constant wave field, \( U_0^n \) is a constant.
A forward-backward scheme was chosen to get a more stable numerical solution.
The centred differences also give a more accurate scheme.

The parameters \( \hr \), \( \epsilon \), \(\alpha\) and \(\gamma\) allow tuning of the model.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../../main"
%%% End:
