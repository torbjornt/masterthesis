% !TeX root = ../../main.tex
% !TeX spellcheck = en_GB


\section{One dimensional description}

A more complete description of the system can be given by introducing an \( x \)-dependency for \( V \), \( \zeta \), \( u \) and \( B \), see \cref{fig:1D-schematic}.
The \( x \)-axis is placed parallel to the reef, with positive direction east-north-east (cf. \cref{fig:instrument-pos}).
Further, the cross-reef volume transport is divided in two terms, where one describes the transport by waves, and the other transport caused by sea level differences across the reef. 
As in the \mD{0}-case, \( \zeta \) and \(\eta\) are not the total water depth, but the deviation from a mean water level (\cref{fig:crossection}).
Where \( \zeta \) varies in both time and space, \( \eta \) is assumed to vary only in time.
\Cref{tab:lagoonparameters-1d} has a summary of the parameters defining the \mD{1} model lagoon.


\begin{table}
  \centering
  \caption{Parameters describing the \mD{1}-system.}
  \label{tab:lagoonparameters-1d}
  \begin{tabular}{>{\(}l<{\)}l}
  \toprule
  \multicolumn{1}{l}{Parameter} & Description                                       \\ \midrule
   V                            & Cross-reef volume transport per unit length       \\
   B                            & Width of lagoon                                   \\
   \zeta                        & Sea-level inside the lagoon, deviation from mean  \\
   \eta                         & Sea-level outside the lagoon, deviation from mean \\
   u                            & Alongshore current velocity inside the lagoon                \\
   H_\mathrm{in}                & Mean water depth inside the lagoon                \\
   H_\mathrm{out}               & Mean water depth outside the lagoon               \\
   h_b                          & Lower limit of \( \eta \) for inflow              \\
   h_t                          & Limit of \(\eta\) for \emph{maximum} inflow \\
   h'_b                         & Lower limit for pressure driven cross-reef flow   \\
   h'_t                         & Limit for \emph{maximum} cross-reef flow   \\
   \bottomrule
\end{tabular}
\end{table}

\begin{figure}
  \centering
  \includestandalone{figures/mathmod/1D-schematic}
  \caption{Schematic of the system, with variations in \( x \).}
  \label{fig:1D-schematic}
\end{figure}


\subsection{Conservation of volume}

\begin{figure}
  \centering
  \includestandalone{figures/mathmod/1D-continuity}
  \caption{Schematic illustration of terms in continuity equation.}
  \label{fig:1D-continuity}
\end{figure}

\Cref{fig:1D-continuity} illustrates a cross section of the lagoon, with length \( \Delta x \), and width \( B(x) \).
Assuming no flow onto, or off, the beach, there is no contribution from the beach side.
That leaves three terms, labelled \( I \), \( II \) and \( III \) in the figure.
The volume transport into the cross section from the left is given by the sea level \( \zeta + H_\mathrm{in} \), width \( B \) and current velocity \( u \), all at position \( x \).
Similarly for the flow out, at position \( x + \Delta x \).
As for the previous case, \( V \) is cross-reef transport per unit length.
This gives the contribution to change in volume of the cross section for the three terms, per unit time:
%
\begin{equation}\label{eq:1D-contterms}
\begin{aligned}
   I:& & [(\zeta + H_\mathrm{in}) Bu](x) \\
  II:& & -[(\zeta + H_\mathrm{in})Bu](x + \Delta x) \\
 III:& & V(x)\Delta x
\end{aligned}
\end{equation}

Analogous to the \mD{0}-case, the change in volume will also be given by
%
\begin{equation}\label{eq:1D-volumechange}
  \Delta \mathcal{V}_{\mathrm{cross\ section}} = \Delta x B(x)\Delta \zeta,
\end{equation}
%
as the mean water depth \( H_\mathrm{in} \) is assumed constant.
By equating the sum of the three terms in \cref{eq:1D-contterms} with the right side of \cref{eq:1D-volumechange}, and defining \( \Gamma = (\zeta + H_\mathrm{in})Bu \), the result is
%
\begin{equation}
  \Delta\zeta B(x)\Delta x = \bigl\{V(x)\Delta x -  
      \bigl[\Gamma(x+\Delta x) - \Gamma(x)\bigr]\bigr\}\Delta t.
\end{equation}
%
Dividing by \( B\Delta x \Delta t \) we get, for small \( \Delta t \), \( \Delta x \),
%
\begin{equation}\label{eq:1d-zeta-final}
  \frac{\partial \zeta}{\partial t} = \frac{1}{B}
      \left[V - \frac{\partial\Gamma}{\partial x}\right],
\end{equation}
%
where \( V \) is given as a function of the incoming wave field and \(\eta\).

The term \( V \) which describes the cross reef mass flux, is split in two terms, one related to the mass transport by the waves, and one related to the difference in sea level between the lagoon and the sea outside the reef.
Hence, we write
%
\begin{equation}
V = \widetilde{V} + \widehat{V},
\end{equation}
%
where \( \widetilde{V} \) is the influx due to waves, and \( \widehat{V} \) is a cross reef flux driven by the cross reef sea level difference.
The latter can cause flow either into or out of the lagoon, depending on where the sea level is higher.

The former, which only cause flow into the lagoon, is a function of the wave field and \(\eta\), where the wave dependency is given by the vertically integrated Stokes velocity, \( M \) (cf. \cref{eq:volumetransport}).
Hence,
%
\begin{equation}
  \label{eq:1D-waveflux}
  \widetilde{V} = \epsilon M\cdot F_{\mathrm{in}}(\eta),
\end{equation}
%
where \( \epsilon \) is a dimensionless coefficient used to tune the model, similar to that seen in the \mD{0} model (\cref{eq:zeroD-V}).

Unlike the \mD{0} case, the \(\eta\) dependency is a dimensionless function \( F_\mathrm{in}(\eta) \) defined as
%
\begin{equation}\label{eq:F(eta)}
  F_\mathrm{in}(\eta) =
   \begin{cases}
     1, & \eta > h_t \\
     \left(\frac{\eta - h_b}{h_t-h_b}\right)^2, & h_b \leq \eta \leq h_t \\
     0, & \eta < h_b
   \end{cases}
\end{equation}
%
Hence, when \(\eta\) is below some lower level \( h_b \), there is no inflow, when \(\eta\) is above some upper level \( h_t \), there is maximum inflow.
That is, for \( \eta > \hr \) there is no further increase of \( \widetilde{V} \), given constant wave heights and periods.
Between the two levels, there is a quadratic increase, schematically described in \cref{fig:massflux-etalevels}.
The values for \( h_b \) and \( h_t \) are given relative to the mean sea level.

\begin{figure}
  \centering
  \includestandalone{figures/mathmod/massflux}
  \caption{Schematic view of \( F(\eta) \).
  The figure does not necessarily reflect the real relationship between the reef height and \( h_b, h_t \).}
  \label{fig:massflux-etalevels}
\end{figure}


The second term, \( \widehat{V} \),	is given as
%
\begin{equation}\label{eq:crossreef_pressureflux}
  \widehat{V} = \lambda \cdot (\eta-\zeta) \cdot F_\mathrm{out}(\mean{\zeta}),
\end{equation}
%
where \(  \lambda \) is a free parameter used to tune the model.
It has unit \si{\m\per\s}, and could perhaps be considered as a representative speed of this cross reef flux.
\( F_\mathrm{out}(\mean{\zeta}) \) is defined similarly to \( F_\mathrm{in}(\eta) \), but with a linear dependency on \( \eta \) between two levels \( h'_b \) and \( h'_t \):
%
\begin{equation}\label{eq:fout(eta)}
  F_\mathrm{out}(\mean{\zeta}) =
   \begin{cases}
     1, & \mean{\zeta} > h'_t \\
     \frac{\mean{\zeta} - h'_b}{h'_t-h'_b}, & h'_b \leq \mean{\zeta} \leq h'_t \\
     0, &  \mean{\zeta} < h'_b
   \end{cases},
   \quad\mean{\zeta} = \frac{\zeta+\eta}{2}
\end{equation}
%
One can think of \( F_{\mathrm{out}} \) as being related to a permeability of the top part of the reef.

This term can cause either flow into, or out of, the lagoon, depending on whether \(\zeta\) or \(\eta\) is larger.
As with \( h_b \) and \( h_t \), the values for \( h'_b \) and \( h'_t \) are given relative to the mean sea level.

\subsection{Momentum}

As in the \mD{0} case, the density is assumed constant.
By assuming hydrostatic balance, we can write the pressure gradient term in terms of the sea level, \( \zeta \).
Hence,
%
\begin{equation}
  - \frac{1}{\rho} \frac{\partial p}{\partial x} = -g\frac{\partial \zeta}{\partial x}.
\end{equation}
%
The frictional term is parameterised as in the \mD{0} case, \ie{} \cref{eq:0d-friction}.
Rotation is ignored, and with no vertical motion, the momentum equation for the \( x \)-direction becomes (\cref{sec:equation-motion})
%
\begin{equation}\label{eq:1d-momentum-final}
  \frac{\partial u}{\partial t} + u\frac{\partial u}{\partial x} =
     -g\frac{\partial\zeta}{\partial x} - ru,
\end{equation}
%

Effects of wind are neglected, as it from observations appears that other factors dominate over the wind.
The current usually runs in the same direction regardless of wind direction.


\subsection{Boundary conditions}

To solve this system of equations, appropriate boundary conditions must be given.
As the western edge of the model lagoon is closed, the velocity at this boundary must be zero.
Further, the sea level in the opening of the reef must be the same as the sea level outside the reef, as discontinuities in the water level cannot occur.
Hence, we get
%
\begin{align}
  u(x=0,t)     &= 0 \label{eq:u-boundary-cond}\\
  \zeta(x=L,t) &= \eta(t). \label{eq:zeta-boundary-cond}
\end{align}





\subsection{Discretisation}

For the discretisation of \cref{eq:1d-zeta-final,eq:1d-momentum-final} staggering in both space and time is applied, as illustrated in \cref{fig:1D-grid}, and as with the \mD{0} case, a forward-backward scheme is used.
The forward step is used on the equation for \( \PDt{\zeta}{t} \), and a backward step for \( \PDt{u}{t} \).
In addition, the frictional term in the momentum equation is placed at time \( n+1 \), giving the following \glspl{fde}.
%
\begin{align}
  \frac{\zeta_j^{n+1} - \zeta_j^n}{\Delta t} &=
  \begin{multlined}[t]
      \frac{1}{B_{2j-1}}\Biggl[
         V_j^n -{} \\
         \qquad \frac{1}{\Delta x}
         \biggl(
           \bigl[0.5(\zeta_{j+1}^n+\zeta_j^n) + H_{\mathrm{in}}\bigr]B_{2j}u_{j}^n - {} \\
           \qquad\qquad \bigl[0.5(\zeta_{j}^n+\zeta_{j-1}^n) + H_{\mathrm{in}}\bigr]B_{2j-2}u_{j-1}^n
         \biggr)
      \Biggr]
  \end{multlined}\label{eq:1D-zetaFDE} \\
  \frac{u_j^{n+1} - u_j^n}{\Delta t} &= 
      - u_j^n\frac{u_{j+1}^n - u_{j-1}^n}{2\Delta x}
      - g \frac{\zeta_{j+1}^{n+1} - \zeta_j^{n+1}}{\Delta x}
      - ru_j^{n+1} \label{eq:1D-uFDE}
\end{align}

\begin{figure}
 \centering
 \subbottom[Grid cell]{\includestandalone{figures/numericalmod/gridcell}}\hfill
 \subbottom[Example grid]{\includestandalone{figures/numericalmod/gridview}}
 \caption[Schematic of \mD{1} model grid.]%
     {Schematics of \mD{1} model grid.
     \( V \) and \( \zeta \) are co-located, \( u \) is staggered a half grid length to the right,      and a half time step after, \( \zeta \) and \( V \).}
  \label{fig:1D-grid}
\end{figure}

Due to the staggered grid, \(\zeta\) is interpolated to \( u \)-points in the continuity equation, \cref{eq:1D-zetaFDE}. 
The width \( B \) is defined at all grid points, which makes any interpolation unnecessary, and gives indices of the form \( 2j-k \).

The friction is placed at time step \( n+1 \) to ensure that it is always directed against the current.

For the first grid cell, the last term in \cref{eq:1D-zetaFDE} and the second term in the numerator of the advection term vanishes because of the boundary condition \cref{eq:u-boundary-cond}.
For the last grid cell, \( \zeta_{j+1} = \eta \), due to the boundary condition, \cref{eq:zeta-boundary-cond}.

Also in the last cell,  \( \PDt{u}{x} \) in the advection term is approximated by a one sided difference, rather than a centred difference.
Hence, \( u_{j+1} \) is not needed.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../../main"
%%% ispell-local-dictionary: "british"
%%% End:
